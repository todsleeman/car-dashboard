import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import firebase from './fire';
import fusioncharts from 'fusioncharts';
import charts from 'fusioncharts/fusioncharts.charts';
import ReactFC from 'react-fusioncharts';
import theme from 'fusioncharts/themes/fusioncharts.theme.candy';
import Money from 'js-money';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Switch from "react-switch";
import Loader from 'react-loader-spinner'
import './App.css';
import 'bootstrap-4-grid/css/grid.min.css';

charts(fusioncharts)
theme(fusioncharts)

class App extends Component {
  constructor(props) {
    super(props);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      msg: "Loading...",
      loaderClass: 'loader',
      show: false,
      usd: false
    };
  }

  componentDidMount(){
    let data = firebase.database().ref('mpg');
    let self = this
    data.on('value', function(snapshot) {
      console.log(snapshot.val())
      if (snapshot.hasChildren()) {
        let chartConfig = generateChartConfig(snapshot, "mscombidy2d");
        ReactDOM.unmountComponentAtNode(document.getElementById('chart-viewer'));
        ReactDOM.render(<ReactFC {...chartConfig} />, document.getElementById('chart-viewer'));
        setTimeout(function() {
          self.setState({loaderClass: 'loader fade-out'})
          setTimeout(function() {
            self.setState({loaderClass: 'loader hide'})
          }, 400)
        }, 700)
      }
    });
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ show: false });
    const form = e.target;
    const mpg = calculateMpg(form.elements, this.state.usd)
    const price = Money.fromDecimal(form.elements.price.value, 'CAD')
    const total_cost = price.multiply(form.elements.consumption.value)
    let mpg_value = {
      distance: form.elements.distance.value,
      consumption: form.elements.consumption.value,
      mpg: mpg,
      price: form.elements.price.value,
      total_cost: total_cost.toDecimal(),
      notes: form.elements.notes.value,
      usd: this.state.usd
    }
    firebase.database().ref('mpg').push( mpg_value );
  }

  handleCurrencyChange(usd) {
    this.setState({ usd: usd });
  }

  render() {
    let consumption;
    let price;
    if (this.state.usd === false){
      consumption = 'litres'
      price = '/litre'
    } else {
      consumption = 'gallons'
      price = '/gallon'
    }
    return (
      <div>
        <div className="chart" id="chart-viewer"></div>
        <Button variant="primary" onClick={this.handleShow} block>
          Fill Up!
        </Button>
        <div className={this.state.loaderClass} ref="loader">
          <div className="spinner">
            <Loader
              color="white"
              height="100"
              width="100"
            />
          </div>
        </div>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Show me the data...</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={e => this.handleSubmit(e)}>
              <Form.Group controlId="distance">
                <label>Distance</label>
                <InputGroup className="distance">
                  <FormControl
                    placeholder="0"
                    type="number"
                    step="0.1"
                    aria-describedby="distance"
                    required
                  />
                  <InputGroup.Append>
                    <InputGroup.Text id="distance">km</InputGroup.Text>
                  </InputGroup.Append>
                </InputGroup>
              </Form.Group>
              <Form.Group controlId="consumption">
                <label>Consumption</label>
                <InputGroup className="consumption">
                  <FormControl
                    placeholder="0.00"
                    type="number"
                    step="0.01"
                    aria-describedby="consumption"
                    required
                  />
                  <InputGroup.Append>
                    <InputGroup.Text id="consumption">{consumption}</InputGroup.Text>
                  </InputGroup.Append>
                </InputGroup>
              </Form.Group>
              <Form.Group controlId="price">
                <label>Price</label>
                <InputGroup className="price">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="price">$</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    placeholder="0.00"
                    type="number"
                    aria-describedby="price"
                    required
                    step="0.01"
                  />
                  <InputGroup.Append>
                    <InputGroup.Text id="price">{price}</InputGroup.Text>
                  </InputGroup.Append>
                </InputGroup>
              </Form.Group>
              <Form.Group controlId="notes">
                <Form.Label>Notes</Form.Label>
                <Form.Control as="textarea" rows="3" type="text" placeholder="What type of driving where you doing?"/>
              </Form.Group>
              <Form.Group controlId="submit">
                <label>
                  <div>
                    USD
                  </div>
                  <Switch
                    id="usd"
                    checked={this.state.usd}
                    onChange={this.handleCurrencyChange}
                    uncheckedIcon={false}
                    checkedIcon={false}
                    boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                    activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                    height={20}
                    width={48}
                    className="react-switch"
                  />
                </label>
                <Button className="submit" variant="primary" type="submit">
                  Submit
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

function generateChartConfig(data, chartType){

  const chartAttr = {
    caption: "Car Fuel Efficiency",
    yaxisname: "Fuel Efficiency (MPG)",
    syaxisname: "Distance Travelled (km)",
    showvalues: "1",
    showanchors: "1",
    plothighlighteffect: "fadeout",
    theme: "candy",
    showhovereffect: "1",
    plotcolorintooltip: "1",
    snumbersuffix: "km"
  };

  let category = [

  ]
  let mpg = {
    seriesname: "MPG",
    renderas: "line",
    showvalues: "0",
    data: []
  }
  let distance = {
    seriesname: "Distance",
    renderas: "bar",
    parentyaxis: "S",
    showvalues: "0",
    color: "#492b5c",
    data: []
  }

  data.forEach((child) => {
    let r = child.val()
    category.push({label: " " })
    let mpg_value = {value: r.mpg, toolText: "MPG: $dataValue"}
    if (r.notes) mpg_value.toolText = r.notes + "<br> " + mpg_value.toolText
    mpg.data.push(mpg_value)
    distance.data.push({value: r.distance, toolText: "Distance: $dataValue"})
  })

  const dataSource = {
    chart: chartAttr,
    categories: [{category}],
    dataset: [mpg, distance]
  };

  let chartHeight = window.innerHeight - 43

  let carChartConfigs = {
    type: chartType,
    width:'100%',
    height: chartHeight,
    dataFormat: 'json',
    dataSource: dataSource
  };

  return carChartConfigs;
}

function calculateMpg({ distance, consumption}, usd) {
  let miles = distance.value / 1.6
  let gallons = consumption.value
  if (usd === false) {
    gallons = gallons / 3.78541
  }
  return miles/gallons
}

export default App;